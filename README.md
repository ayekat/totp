totp
====

`totp` is a tool for easily generating time-based one-time passwords (TOTPs)
used for multi-factor authentication (MFA) on the majority of online services.
It uses [`pass`][passws] to manage the TOTP secrets, and [`oathtool`][oathtk] to
generate the TOTPs from those secrets.

`totpmenu` is a dmenu wrapper for more interactively selecting (and optionally
auto-typing) a TOTP, similar to `passmenu`.


Dependencies
------------

* `/bin/sh` (e.g. ash, bash, dash, zsh, ...)
* `cat`, `cut`, `id`, `mktemp`, `printf`, `sort`, `test` (e.g. GNU coreutils)
* `find` (e.g. GNU findutils)
* `getent` (GNU libc)
* `oathtool` (oath-toolkit)
* `pass`

Further dependencies for optional functionality:

* `xclip` (for `totp -c`)
* `notify-send` (libnotify, for X notifications with `totp -c`)
* `scrot` (for `totp scan`)
* `zbarimg` from ZBar (for `totp scan`)

Further dependencies for `totpmenu`:

* `dmenu`
* `xclip` (for `totpmenu` without `-t`)
* `xdotool` (for `totpmenu -t`)


Installation
------------

`totp` is a single POSIX sh script that can be installed to some place in
`$PATH` as-is (or simply run from this project directory).

`totpmenu` is a single POSIX sh script that can be installed to some place in
`$PATH` as-is as well; it expects `totp` to be already present in `$PATH`.

The manpages are typeset in Groff and can be installed as-is (or simply be read
in this project directory with `man ./totp.1` and `man ./totpmenu.1`,
respectively).

Arch Linux users may also find [this PKGBUILD][pkgbuild] useful.


Usage
-----

Initialise a password store for `totp` in `$XDG_STATE_HOME/totp` and set the GPG
key that shall be used to encrypt the secrets:

```
$ totp init john.doe@thedoes.tld
Password store initialized for john.doe@thedoes.tld
```

Add a new TOTP secret (if the secret is omitted, it can be entered
interactively, to hide it from the shell history):

```
$ totp add cloud WAR4X3N9Z9TTFE1S
```

List all known TOTP secrets:

```
$ totp list
Password Store
└── cloud
```

Generate a TOTP from a secret:

```
$ totp show cloud
243420
```

Generate a new TOTP after it has rotated (every 30 seconds):

```
$ totp show cloud
155941
```

Display the raw secret:

```
$ totp show -r cloud
WAR4X3N9Z9TTFE1S
```

Scan an area of the screen containing a QR code, and add the secret:

```
$ totp scan foobar
```


Author
------

`totp` was written by Tinu "ayekat" Weber in a fairly locked down 2020.

`totp` is licenced under the GNU General Public License, version 3 (see
[`COPYING`][copying] for more information).


Thanks
------

Special thanks to the authors of `pass` and the authors of `oathtool`.


[oathtk]: https://www.nongnu.org/oath-toolkit/
[passws]: https://www.passwordstore.org/
[pkgbuild]: https://gitlab.com/ayekat/PKGBUILDs/-/blob/totp-git/PKGBUILD
[copying]: COPYING
